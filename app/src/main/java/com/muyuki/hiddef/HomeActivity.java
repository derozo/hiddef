package com.muyuki.hiddef;

import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //No action Bar and full screen
        if(getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_home);

        setFonts ();

    }

    public void setFonts (){
        //Fonts
        Typeface bold = Typeface.createFromAsset(getAssets(), "fonts/Comfortaa-Bold.ttf");
        Typeface regular = Typeface.createFromAsset(getAssets(), "fonts/Comfortaa-Bold.ttf");
        Typeface light = Typeface.createFromAsset(getAssets(), "fonts/Comfortaa-Bold.ttf");
        TextView hiddefHomeTitle =  (TextView) findViewById(R.id.hiddefHomeTitle);

        hiddefHomeTitle.setTypeface(bold);

        TextView loginHomeTitle = (TextView) findViewById(R.id.loginHomeTitle);
        loginHomeTitle.setTypeface(bold);

        TextView useEmailText =  (TextView) findViewById(R.id.useEmailText);
        useEmailText.setTypeface(bold);

        TextView bySinginText = (TextView) findViewById(R.id.bySinginText);
        bySinginText.setTypeface(bold);

        TextView tosText =  (TextView) findViewById(R.id.tosText);
        tosText.setTypeface(bold);

        Button facebookButton =  (Button) findViewById(R.id.facebookButton);
        facebookButton.setTypeface(bold);

        TextView googleButton = (Button) findViewById(R.id.googleButton);
        googleButton.setTypeface(bold);

        Button instagramButton =  (Button) findViewById(R.id.instagramButton);
        instagramButton.setTypeface(bold);

        TextView twitterButton = (Button) findViewById(R.id.twitterButton);
        twitterButton.setTypeface(bold);

        Button loginButton =  (Button) findViewById(R.id.loginButton);
        loginButton.setTypeface(bold);

        TextView signupButton = (Button) findViewById(R.id.signupButton);
        signupButton.setTypeface(bold);
    }

    public void launchFacebookLogin (View view){
        Intent intent = new Intent(this, FacebookActivity.class);
        startActivity(intent);
    }

    public void launchTos (View view){
        Intent intent = new Intent(this, TosActivity.class);
        startActivity(intent);
    }

}
