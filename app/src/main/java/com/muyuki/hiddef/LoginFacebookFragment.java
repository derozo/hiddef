package com.muyuki.hiddef;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.w3c.dom.Text;

import static android.R.attr.data;
import static com.facebook.FacebookSdk.getApplicationContext;

public class LoginFacebookFragment extends Fragment {

    private CallbackManager callbackManager ;
    private LoginButton loginButton;
    private ProfileTracker profileTracker;
    private AccessTokenTracker accessTokenTracker;

    public LoginFacebookFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                //Aply all methods with profile user data
                displayWellcomeMessage(newProfile);
            }
        };
        accessTokenTracker.startTracking();
        profileTracker.startTracking();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login_facebook, container, false);

        loginButton = (LoginButton) view.findViewById(R.id.login_facebook_button);
        loginButton.setReadPermissions("user_friends");

        //loginButton.setReadPermissions("email");
        // If using in a fragment
        loginButton.setFragment(this);

        // Other app specific specialization

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                AccessToken accessToken = loginResult.getAccessToken();
                //profileTracker profileTracker = new ProfileTracker();
                Profile profile = Profile.getCurrentProfile();
                displayWellcomeMessage(profile);
             }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(getApplicationContext(), "Cancelando",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(getApplicationContext(), exception.toString(),Toast.LENGTH_LONG).show();

            }
        });

        return view;
    }

    public void displayWellcomeMessage(Profile profile) {
        if (profile != null) {
            Toast.makeText(getApplicationContext(), "Bienvenido ... "+ profile.getName(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayWellcomeMessage(profile);
    }

    public void onStop(){
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


}
